Summary: Virtual Distributed Storage network
Name: vds
Version: 0.1
Release: 2%{?dist}
License: MIT
Group: System
AutoReq: no
AutoReqProv: no
AutoProv: no

%description 
The Virtual Distributed Storage network is free distributed storage network for survival data.
Anonymous, encrypted, foreverc.

%define source_dir %{buildroot}/../../../../dist/
%define target_dir /usr/share/vds/

%install 
mkdir -p %{buildroot}/%{target_dir}
cp %{source_dir}vds_ws_server %{buildroot}/%{target_dir}vds_ws_server
cp %{source_dir}libc++.so.1 %{buildroot}/%{target_dir}libc++.so.1
cp %{source_dir}libc++.so %{buildroot}/%{target_dir}libc++.so
cp %{source_dir}libc++abi.so.1 %{buildroot}/%{target_dir}libc++abi.so.1
cp %{source_dir}libcrypto.so.1.1 %{buildroot}/%{target_dir}libcrypto.so.1.1
cp %{source_dir}libssl.so.1.1 %{buildroot}/%{target_dir}libssl.so.1.1
cp %{source_dir}BouncyCastle.Crypto.dll %{buildroot}/%{target_dir}BouncyCastle.Crypto.dll
cp %{source_dir}CommandLine.dll %{buildroot}/%{target_dir}CommandLine.dll
cp %{source_dir}IVySoft.VPlatform.Network.dll %{buildroot}/%{target_dir}IVySoft.VPlatform.Network.dll
cp %{source_dir}IVySoft.VDS.Client.Cmd %{buildroot}/%{target_dir}IVySoft.VDS.Client.Cmd
cp %{source_dir}Microsoft.AspNetCore.Cryptography.Internal.dll %{buildroot}/%{target_dir}Microsoft.AspNetCore.Cryptography.Internal.dll
cp %{source_dir}IVySoft.VDS.Client.Cmd.deps.json %{buildroot}/%{target_dir}IVySoft.VDS.Client.Cmd.deps.json
cp %{source_dir}IVySoft.VPlatform.Utils.dll %{buildroot}/%{target_dir}IVySoft.VPlatform.Utils.dll
cp %{source_dir}Microsoft.AspNetCore.Cryptography.KeyDerivation.dll %{buildroot}/%{target_dir}Microsoft.AspNetCore.Cryptography.KeyDerivation.dll
cp %{source_dir}IVySoft.VDS.Client.Cmd.dll %{buildroot}/%{target_dir}IVySoft.VDS.Client.Cmd.dll
cp %{source_dir}Newtonsoft.Json.dll %{buildroot}/%{target_dir}Newtonsoft.Json.dll
cp %{source_dir}IVySoft.VDS.Client.Cmd.runtimeconfig.json %{buildroot}/%{target_dir}IVySoft.VDS.Client.Cmd.runtimeconfig.json
cp %{source_dir}IVySoft.VDS.Client.dll %{buildroot}/%{target_dir}IVySoft.VDS.Client.dll

%files
%{target_dir}vds_ws_server
%{target_dir}libc++.so.1
%{target_dir}libc++.so
%{target_dir}libc++abi.so.1
%{target_dir}libcrypto.so.1.1
%{target_dir}libssl.so.1.1
%{target_dir}BouncyCastle.Crypto.dll
%{target_dir}CommandLine.dll
%{target_dir}IVySoft.VPlatform.Network.dll
%{target_dir}IVySoft.VDS.Client.Cmd
%{target_dir}Microsoft.AspNetCore.Cryptography.Internal.dll
%{target_dir}IVySoft.VDS.Client.Cmd.deps.json
%{target_dir}IVySoft.VPlatform.Utils.dll
%{target_dir}Microsoft.AspNetCore.Cryptography.KeyDerivation.dll
%{target_dir}IVySoft.VDS.Client.Cmd.dll
%{target_dir}Newtonsoft.Json.dll
%{target_dir}IVySoft.VDS.Client.Cmd.runtimeconfig.json
%{target_dir}IVySoft.VDS.Client.dll

%post
%{__ln_s} -f %{target_dir}IVySoft.VDS.Client.Cmd %{_bindir}/IVySoft.VDS.Client.Cmd

%postun
%{__rm} -f %{_bindir}/IVySoft.VDS.Client.Cmd

%changelog
* Sun Oct 25 2020 IVy Soft <contact@iv-soft.ru> 0.1
- Initial version of the package
ORG-LIST-END-MARKE

