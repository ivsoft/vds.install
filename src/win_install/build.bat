set PATH=%PATH%;..\..\..\wix\
set version=0.0.3

set channel_name=windows.debug.en-us
echo {"channel_name":"%channel_name%","version":"%version%"} > out\IVySoft.VDS.Client.AutoUpdate.exe.json
candle.exe -ext WixUIExtension -ext WixUtilExtension -dLanguage=1033 -dSourceDir=..\..\..\vds\build -dSourceDirTools=..\..\..\vds.net -dConfig=Debug -dCulture=en-us -dProductVersion=%version% vds.wxs -out out\vds-en.wixobj
light.exe -ext WixUIExtension -ext WixUtilExtension out\vds-en.wixobj -cultures:en-us -loc vds.en-us.wxl -out out\vds-en-us.msi

set channel_name=windows.debug.ru-ru
echo {"channel_name":"%channel_name%","version":"%version%"} > out\IVySoft.VDS.Client.AutoUpdate.exe.json
candle.exe -ext WixUIExtension -ext WixUtilExtension -dLanguage=1049 -dSourceDir=..\..\..\vds\build -dSourceDirTools=..\..\..\vds.net -dConfig=Debug -dCulture=ru-ru -dProductVersion=%version% vds.wxs -out out\vds-ru.wixobj
light.exe -ext WixUIExtension -ext WixUtilExtension out\vds-ru.wixobj -cultures:ru-ru -loc vds.ru-ru.wxl -out out\vds-ru-ru.msi